"""Stream type classes for tap-currencylayer."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_currencylayer.client import CurrencylayerStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class CurrencyStream(CurrencylayerStream):
    """Define custom stream."""
    name = "currency"
    path = "/live"
    primary_keys = ["id"]
    #replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("success", th.BooleanType),
        th.Property("terms", th.StringType),
        th.Property("privacy", th.StringType),
        th.Property("timestamp", th.NumberType),
        th.Property("source", th.StringType),
        th.Property("quotes", th.CustomType({"type": ["object", "string"]})),
        
    ).to_dict()
