"""Currencylayer tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_currencylayer.streams import (
    CurrencylayerStream,
    CurrencyStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    CurrencyStream,
]


class TapCurrencylayer(Tap):
    """Currencylayer tap class."""
    name = "tap-currencylayer"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The API key to authenticate against the API service"
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="http://api.currencylayer.com",
            description="The url for the API service"
        ),
        th.Property(
            "base",
            th.StringType,
            default="USD",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == '__main__':
    TapCurrencylayer.cli()
